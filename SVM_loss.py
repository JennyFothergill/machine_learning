import numpy as np


scores = np.array(
        [[3.2, 1.3, 2.2],
         [5.1, 4.9, 2.5],
         [-1.7,2.0,-3.1]]
        )

def L_i_vectorized(x, y, scores): #W):
    #scores = W.dot(x)
    a = scores[:,x] - scores[y,x] + 1
    margins = np.where(a > 0, a, 0)
    margins[y] = 0
    loss_i = np.sum(margins)
    return loss_i
