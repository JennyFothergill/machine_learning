from builtins import range
import numpy as np
from random import shuffle
#from past.builtins import xrange

def softmax_loss_naive(W, X, y, reg):
    """
    Softmax loss function, naive implementation (with loops)

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using explicit loops.     #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    # compute the loss and the gradient
    num_classes = W.shape[1]
    num_train = X.shape[0]
    num_dims = W.shape[0]
    for i in range(num_train):
        # 1xD * DxC is 1xC
        scores = X[i,:].dot(W)

        # shift scores down by max to prevent numerical instability
        scores -= max(scores)

        exp_scores = np.exp(scores)

        # normalize so that probabilities sum to one
        p_scores = exp_scores/np.sum(exp_scores)

        loss += -np.log(p_scores[y[i]])

        for d in range(num_dims):
            for c in range(num_classes):
                # if the class is the correct class for the image
                if c == y[i]:
                    dW[d,c] += X[i,d] * (p_scores[c] - 1)
                else:
                    dW[d,c] += X[i,d] * p_scores[c]

    # Average and regularization are the same as SVM
    # Right now the loss is a sum over all training examples, but we want it
    # to be an average instead so we divide by num_train.
    loss /= num_train

    # Add regularization to the loss.
    loss += 0.5 * reg * np.sum(W * W)

    # Average and add regularization to W as well
    dW /= num_train
    dW += reg * W

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
    """
    Softmax loss function, vectorized version.

    Inputs and outputs are the same as softmax_loss_naive.
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    # compute the loss and the gradient
    num_classes = W.shape[1]
    num_train = X.shape[0]
    num_dims = W.shape[0]

    # NxD * DxC is NxC
    scores = X.dot(W)

    # shift scores of each class down by max of that class
    # to prevent numerical instability
    scores -= np.max(scores, axis=1)[:,np.newaxis]

    exp_scores = np.exp(scores)

    # normalize so that probabilities sum to one
    # divide by column-wise sum
    p_scores = exp_scores/np.sum(exp_scores, axis=1)[:, np.newaxis]

    # p_scores is NxC, y[i] is the correct class label for image i
    p_correct = p_scores[[i for i in range(num_train)], y]

    # the loss then is the sum of the negative logs of the
    # probabilities of of the (normalized) correct classes
    loss = np.sum(-np.log(p_correct))

    # average and regularization, same as before
    loss /= num_train
    loss += 0.5 * reg * np.sum(W * W)

    # subtract one from all the correct probabilities
    p_scores[[i for i in range(num_train)], y] -= 1

    # dW DxC, p_scores NxC, X NxD
    # X dot W gives us scores, so p_scores dot X gives us dW
    # but need to transpose: X.T (DxN) dot (NxC) is (DxC)
    dW = np.dot(X.T, p_scores)

    # Average and add regularization just like before
    dW /= num_train
    dW += reg * W

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW
